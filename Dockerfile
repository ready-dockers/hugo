FROM ubuntu
MAINTAINER in.pratik13@gmail.com

ENV HUGO_VERSION 0.54.0
ENV HUGO_DOWNLOAD_URL v${HUGO_VERSION}/hugo_extended_${HUGO_VERSION}_Linux-64bit.tar.gz

WORKDIR /tmp

RUN apt-get update
RUN apt-get -y install wget

RUN wget -O hugo.tar.gz https://github.com/gohugoio/hugo/releases/download/${HUGO_DOWNLOAD_URL}
RUN tar -zxf hugo.tar.gz
RUN rm hugo.tar.gz
RUN chmod 777 hugo
RUN mv hugo /usr/bin/

RUN hugo version